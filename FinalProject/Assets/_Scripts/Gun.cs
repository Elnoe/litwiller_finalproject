﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Gun : MonoBehaviour
{
    public float damage = 10f;
    public float range = 100f;
    public float fireRate = 15f;
    public float impactForce = 30f;

    public int maxAmmo = 30;
    private int currentAmmo;
    public float reloadTime = 1f;
    private bool isReloading = false;

    public Camera fpsCam;
    public ParticleSystem MuzzleFlash;
    public GameObject impactEffect;

    public Text ammoText;

    private float nextTimeToFire = 0f;

    public Animator animator;

    void Start()
    {
        currentAmmo = maxAmmo; //initialize ammo
        ammoText.text = "";
    }

    void OnEnable()
    {
        isReloading = false;
        animator.SetBool("Reloading", false); //prevent reload animation from playing on game start
    }

    // Update is called once per frame
    void Update()
    {
        if (Instructions.isGameStart)
        {
            ammoText.text = "Ammo: " + currentAmmo.ToString();
            if (isReloading)
            {
                return;
            }

            if (currentAmmo <= 0) //Reload when ammo runs out
            {
                StartCoroutine(Reload());
                return;
            }

            if (Input.GetButton("Fire1") && Time.time >= nextTimeToFire) //Fire if allowed by the current fire rate
            {
                nextTimeToFire = Time.time + 1f / fireRate;
                Shoot();
            }
        }

    }

    IEnumerator Reload()
    {
        GameObject.Find("startReload").GetComponent<AudioSource>().Play(); //Play the start reload sound
        isReloading = true;
        Debug.Log("Reloading...");

        animator.SetBool("Reloading", true); //Start the reload animation

        yield return new WaitForSeconds(reloadTime - .25f); //wait for start reload animation to finish

        animator.SetBool("Reloading", false); //Play the end reload animation

        GameObject.Find("endReload").GetComponent<AudioSource>().Play(); //Play the end reload sound
        yield return new WaitForSeconds(.25f); //wait for end animation to finish

        currentAmmo = maxAmmo; //Set ammo
        if (Instructions.isGameStart)
        {
            ammoText.text = "Ammo: " + currentAmmo.ToString(); //update ammo counter
        }
        isReloading = false;
    }

    void Shoot()
    {
        gameObject.GetComponent<AudioSource>().Play(); //Play gunshot sound effect
        currentAmmo--;
        if (Instructions.isGameStart)
        {
            ammoText.text = "Ammo: " + currentAmmo.ToString(); //update ammo counter
        }

        MuzzleFlash.Play();

        RaycastHit hit;
        if (Physics.Raycast(fpsCam.transform.position, fpsCam.transform.forward, out hit, range)) //check for hit
        {
            Debug.Log(hit.transform.name);

            Target target = hit.transform.GetComponent<Target>();

            if (target != null)
            {
                target.TakeDamage(damage); //deal damage if the hit is on a Target
            }

            if(hit.rigidbody != null) //Add impact force to object hit (not used for this project)
            {
                hit.rigidbody.AddForce(-hit.normal * impactForce);
            }

            GameObject impactGO = Instantiate(impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
            Destroy(impactGO, 2f);
        }
    }
}
