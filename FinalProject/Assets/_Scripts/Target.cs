﻿using UnityEngine;

public class Target : MonoBehaviour
{
    public float health = 50f;

    public void TakeDamage(float amount)
    {
        if(!GameController.isGameEnd)
        {
            health -= amount; //deal damage to the target
            if (health <= 0f)
            {
                GameController.numTargetsLeft--; //subtract one targetfrom the total
                Die(); //destroy the target
            }
        }
    }

    void Die()
    { 
        GameObject.Find("TargetPrefabs").GetComponent<AudioSource>().Play(); //player the target destroyed sound effect
        Destroy(gameObject); 
    }
}
