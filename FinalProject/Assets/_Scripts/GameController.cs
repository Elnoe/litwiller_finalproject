﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static GameController gc;

    public GameObject Targets;
    public GameObject TargetInlineX;
    public GameObject TargetInlineZ;

    public static float time;

    //Note: some HUD elements scale poorly or disappear on smaller window sizes, "Maximize On Play" is recommended when in the game view
    public Text timeText;
    public Text targetsLeftText;
    public Text winText;
    public Text loseText;
    public Text restartText;

    public static int numTargetsLeft;
    public List<BoxCollider> targetZones;
    public Dictionary<string, bool> isAlignedWithX;

    public Transform playerTransform;

    public bool hasPlayedWinSound;
    public bool hasPlayedLoseSound;

    public bool isWin;
    public static bool isGameEnd;

    /*
    private void Awake()
    {
        if (gc == null)
        {
            gc = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }  
    }
    */
    

    void Start()
    {
        hasPlayedWinSound = false;
        hasPlayedLoseSound = false;
        time = 45f;
        isWin = false;
        isGameEnd = false;
        Time.timeScale = 1;
        numTargetsLeft = 0;
        playerTransform = GameObject.Find("Player").GetComponent<Transform>();
        winText.text = "";
        loseText.text = "";
        restartText.text = "";

        setAxisAlignments();

        //Spawn targets
        foreach (BoxCollider b in targetZones)
        {
            Vector3 curr = selectPoint(b); //Select a point in each target zone
            if (isAlignedWithX[b.gameObject.ToString()]) //Spawn target aligned with the X-axis
            {
                Instantiate(TargetInlineX, curr, Quaternion.identity);
                print("Spawned a target at: " + curr + " aligned with x-axis");
            }
            else //Spawn target aligned with Z-axis
            {
                Instantiate(TargetInlineX, curr, Quaternion.AngleAxis(90, Vector3.up));
                print("Spawned a target at: " + curr + " aligned with z-axis");
            }

            numTargetsLeft++; 
            b.gameObject.SetActive(false); //Destroy the associated BoxCollider to prevent hit detection issues
        }
    }

    // Update is called once per frame
    void Update()
    {
        //Quit on ESC press
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        //Restart the game if the game is over and Jump is pressed
        if (isGameEnd && Input.GetButtonDown("Jump"))
        {
            SceneManager.LoadScene("Level");
        }

        //The game starts when the Instructions have disappeared
        if (Instructions.isGameStart)
        {
            targetsLeftText.text = "Targets Left: " + numTargetsLeft; //display the number of targets left

            //Update the time if it is >0 and the game hasn't ended
            if (time >= 0 && !isGameEnd)
            {
                time -= Time.deltaTime;
                if (time < 0) time = 0;
                timeText.text = "Time: " + time.ToString("0.00");
            }

            /*
            if (time <= 5)
            {
                targetsLeftText.fontSize += 7;
                timeText.fontSize += 7;
            }
            else if (time <= 10)
            {
                targetsLeftText.fontSize += 7;
                timeText.fontSize += 7;
            }
            */

            //If time is <= 0 or the player has fallen out of bounds, enter the lose state
            if (!isGameEnd && !isWin && (time <= 0 && numTargetsLeft > 0) || (playerTransform != null && playerTransform.localPosition.y <= 50))
            {
                loseText.text = "You Lose!";
                gameObject.GetComponent<AudioSource>().Stop(); //stop the game music
                if(!hasPlayedLoseSound) //play lose sound once
                {
                    GameObject.Find("Lose").GetComponent<AudioSource>().Play();
                    hasPlayedLoseSound = true;
                }
                isGameEnd = true; //the game ends
                Time.timeScale = 0.1f; //slow time
            }

            //if all targets have been destoryed and there is time left, enter the win state
            if (!isGameEnd && numTargetsLeft == 0 && time > 0)
            {
                winText.text = "You Win!";
                gameObject.GetComponent<AudioSource>().Stop(); //stop the game music
                if (!hasPlayedWinSound) //play win sound once
                {
                    GameObject.Find("Win").GetComponent<AudioSource>().Play();
                    hasPlayedWinSound = true;
                }
                isWin = true;
                isGameEnd = true; //the game ends
                Time.timeScale = 0.1f; //slow time
            }


            if (isGameEnd) //prompt player to restart if the game has ended
            {
                restartText.text = "(Press [SPACE] to Restart)";
            }
        }
    }

    //Sets the axis alignment for each target zone (true = X, false = Z)
    public void setAxisAlignments()
    {
        isAlignedWithX = new Dictionary<string, bool>();
        isAlignedWithX.Add("WallLargeB (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("FloorInteriorB1 (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("FloorInteriorA4 (6) (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("FloorInteriorB1 (6) (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("PanelB (4) (UnityEngine.GameObject)", true);
        isAlignedWithX.Add("PanelB (3) (UnityEngine.GameObject)", true);
        isAlignedWithX.Add("PanelB (2.1) (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("PanelB (2.2) (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("WallLargeB (2) (UnityEngine.GameObject)", false);
        isAlignedWithX.Add("PanelB (1) (UnityEngine.GameObject)", true);
        isAlignedWithX.Add("PanelB (UnityEngine.GameObject)", true);
    }

    //Selects a random point within the given BoxCollider
    public Vector3 selectPoint(BoxCollider b)
    {
        Bounds bounds = b.bounds;
        return new Vector3(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y), Random.Range(bounds.min.z, bounds.max.z));
    }
}
