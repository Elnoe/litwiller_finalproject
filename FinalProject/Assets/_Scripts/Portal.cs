﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour
{
    public Dictionary<Vector3, Vector3> portalPairs;
    public Vector3 playerOffset;
    public Vector3 portalOffset;
    float portalExitDistance;
    public Transform playerTransform;

    // Start is called before the first frame update
    void Start()
    {
        playerOffset = new Vector3(0,0,0);
        //playerOffset = new Vector3(3.139f - 3.46f, 76.142f - 75.86f, 94.219f - 97.76f);

        playerTransform = gameObject.GetComponent<Transform>();

        portalOffset = new Vector3(17.3f, 7f, -8.4f); 
        portalExitDistance = 1.2f; //How far the player lands away from the exit portal

        portalPairs = new Dictionary<Vector3, Vector3>();

        //Setup portal pairs
        portalPairs.Add(new Vector3(-1.959f + portalOffset.x, 70.9f + portalOffset.y, 97.47f + portalOffset.z), 
                        new Vector3(-14.537f + portalOffset.x, 63.53f + portalOffset.y, 86.13f + portalOffset.z - portalExitDistance));
        portalPairs.Add(new Vector3(-14.537f + portalOffset.x, 63.53f + portalOffset.y, 86.13f + portalOffset.z), 
                        new Vector3(-1.959f + portalOffset.x, 70.9f + portalOffset.y, 97.47f + portalOffset.z + portalExitDistance));
        portalPairs.Add(new Vector3(9.974f + portalOffset.x, 68.203f + portalOffset.y, 79.473f + portalOffset.z), 
                        new Vector3(-36.0877f + portalOffset.x + portalExitDistance - 0.5f, 71.89553f + portalOffset.y, 86.23799f + portalOffset.z));
        portalPairs.Add(new Vector3(-36.0877f + portalOffset.x, 71.89553f + portalOffset.y, 86.23799f + portalOffset.z), 
                        new Vector3(9.974f + portalOffset.x - portalExitDistance - 2, 68.203f + portalOffset.y, 79.473f + portalOffset.z));
    }

    // Update is called once per frame
    void Update()
    {
        float range = 0.5f; //how far away the player activates a portal

        foreach (Vector3 v in portalPairs.Keys) //check each portal
        {
            //If a player is within range of a portal and the game has not ended
            if (!GameController.isGameEnd && Mathf.Abs(playerTransform.position.x - v.x + playerOffset.x) < range && Mathf.Abs(playerTransform.position.y - v.y + playerOffset.y) < range &&
                Mathf.Abs(playerTransform.position.z - v.z + playerOffset.z) < range)
            {
                Vector3 endPos = portalPairs[v]; //find the correct exit portal for the portal v
                playerTransform.position = new Vector3(endPos.x, endPos.y, endPos.z); //move the player to the exit
            }
        }
    }
}
