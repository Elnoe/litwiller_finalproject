﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class prevents the instructions from appearing more than once
public class IsFirstRun : MonoBehaviour
{
    public bool isFirstRun = true; //is this the first run of the game?
    void Awake()
    {
        isFirstRun = true;
        DontDestroyOnLoad(gameObject);
    }
}
