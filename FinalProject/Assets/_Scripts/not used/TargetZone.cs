﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetZone : MonoBehaviour
{
    public Vector3 TopLeft;
    public Vector3 TopRight;
    public Vector3 BottomLeft;
    public bool isAlignedWithX;

    public TargetZone(Vector3 TopLeft, Vector3 TopRight, Vector3 BottomLeft, bool isAlignedWithX)
    {
        this.TopLeft = TopLeft;
        this.TopRight = TopRight;
        this.BottomLeft = BottomLeft;
        this.isAlignedWithX = isAlignedWithX;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public Vector3 selectPoint()
    {
        Vector3 result = new Vector3 (0,0,0);

        if (isAlignedWithX)
        {
            result = new Vector3(Random.Range(TopLeft.x, TopRight.x),
                                 Random.Range(BottomLeft.y, TopLeft.y),
                                 TopLeft.z);
        }
        else
        {
            result = new Vector3(TopLeft.x,
                                 Random.Range(BottomLeft.y, TopLeft.y),
                                 Random.Range(TopRight.z, TopLeft.z));
        }

        return result;
    }
}
