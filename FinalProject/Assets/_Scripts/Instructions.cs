﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Instructions : MonoBehaviour
{
    float previousXSens;
    float previousYSens;
    RigidbodyFirstPersonController rbfbcPlayer;
    public static bool isGameStart = false;
    public GameObject reticle;

    // Start is called before the first frame update
    void Awake()
    {
        if (GameObject.Find("IsFirstRun").GetComponent<IsFirstRun>().isFirstRun) //Only show instructions on the first run
        {
            isGameStart = false;
            rbfbcPlayer = GameObject.Find("Player").GetComponent<RigidbodyFirstPersonController>();
            Time.timeScale = 0f; //Freeze time
            rbfbcPlayer.movementSettings.ForwardSpeed = 0f;
            rbfbcPlayer.movementSettings.BackwardSpeed = 0f; //Freeze movement
            rbfbcPlayer.movementSettings.StrafeSpeed = 0f;
            previousXSens = rbfbcPlayer.mouseLook.XSensitivity; //Maintain set sensitivity
            previousYSens = rbfbcPlayer.mouseLook.YSensitivity;
            rbfbcPlayer.mouseLook.XSensitivity = 0f; //Freeze sensitivity
            rbfbcPlayer.mouseLook.YSensitivity = 0f;
            reticle.SetActive(false); //hide reticle
            Invoke("CloseInstructions", 5f); //Call the function to close after 5 seconds
        }
        else //Skip the instructions
        {
            gameObject.SetActive(false);
            isGameStart = true;
        }
    }

        void CloseInstructions()
    {
        GameObject.Find("IsFirstRun").GetComponent<IsFirstRun>().isFirstRun = false;
        Time.timeScale = 1f; //Unpause
        gameObject.SetActive(false); //Hide this gameObject
        rbfbcPlayer.movementSettings.ForwardSpeed = 8f; //Unfreeze movement
        rbfbcPlayer.movementSettings.BackwardSpeed = 8f;
        rbfbcPlayer.movementSettings.StrafeSpeed = 8f;
        rbfbcPlayer.mouseLook.XSensitivity = previousXSens; //Unfreeze sensitivity
        rbfbcPlayer.mouseLook.YSensitivity = previousYSens;
        reticle.SetActive(true); //show reticle
        isGameStart = true; //Lets GameController know game is ready to start
    }
}
