﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.Characters.FirstPerson;

public class Powerup : MonoBehaviour
{
    public string type;

    public Transform playerTransform;
    public Transform thisTransform;

    public Text jumpText;
    public Text timeText;


    // Start is called before the first frame update
    void Start()
    {
        playerTransform = GameObject.Find("Player").GetComponent<Transform>();
        thisTransform = gameObject.GetComponent<Transform>();
        jumpText.text = "";
        timeText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime); //Passive rotating effect

        float range = 1f;

        //If the player is within range and the hasn't been picked up yet
        if (!GameController.isGameEnd && Mathf.Abs(playerTransform.position.x - thisTransform.position.x) < range
            && Mathf.Abs(playerTransform.position.y - thisTransform.position.y) < range
            && Mathf.Abs(playerTransform.position.z - thisTransform.position.z) < range
            && gameObject.GetComponent<MeshRenderer>().enabled)
        {
            //Apply an effect based on the powerup type
            if(type == "jump")
            {
                StartCoroutine(ActivateAndDelete(5));
            }
            else if(type == "time")
            {
                StartCoroutine(ActivateAndDelete(3));
            }
            
        }

    }

    IEnumerator ActivateAndDelete(float amount) //Activates the powerup and deletes it after "amount" seconds
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<BoxCollider>().enabled = false;

        //Activate the powerup
        if (type == "time")
        {
            GameObject.Find("timePowerup").GetComponent<AudioSource>().Play();
            GameController.time += 5f;
            timeText.text = "+5 Time";
            print("Time +5");
        }
        else if(type == "jump")
        {
            GameObject.Find("jumpPowerup").GetComponent<AudioSource>().Play();
            GameObject.Find("Player").GetComponent<RigidbodyFirstPersonController>().movementSettings.JumpForce = 80;
            jumpText.text = "+Jump";
            print("Jump Height x2");
        }

        yield return new WaitForSeconds(amount);
        if (type == "jump")
        {
            GameObject.Find("Player").GetComponent<RigidbodyFirstPersonController>().movementSettings.JumpForce = 50;
        }

        //Clear the HUD element for the powerup
        if(type == "time")
        {
            timeText.text = "";
        }
        else if(type == "jump")
        {
            jumpText.text = "";
        }

        Destroy(gameObject);
        
        Debug.Log("deleted " + type);
    }
}
